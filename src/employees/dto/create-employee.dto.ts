import { IsNotEmpty, IsNumber, Length } from 'class-validator';

export class CreateEmployeeDto {
  @IsNotEmpty()
  @Length(3, 64)
  name: string;
  @IsNotEmpty()
  @Length(3, 32)
  username: string;
  @IsNotEmpty()
  @Length(3, 16)
  password: string;
  @IsNotEmpty()
  birthday: Date;
  @IsNotEmpty()
  address: string;
  @IsNotEmpty()
  @Length(0, 10)
  tel: string;
  @IsNotEmpty()
  email: string;

  other_contact: string;
  @IsNotEmpty()
  date_start: Date;
  @IsNotEmpty()
  role: string;
  @IsNotEmpty()
  @IsNumber()
  rate: number;
}
